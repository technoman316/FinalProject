package finalProject;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.util.Scanner;

public class Login {

	public static void main(String[] args) {

		String User = "username";
		String Pass = "password";
		String entereduname;
		String enteredpass;
		int trials = 1;
		boolean logged = false;

		while (trials <= 3) {
			entereduname = JOptionPane.showInputDialog("Enter Username");
			if (!User.equals(entereduname) && trials < 3) {
				JOptionPane.showMessageDialog(null, "Incorrect Username");
				trials++;
			} else if ((trials == 3) && (!User.equals(entereduname))) {
				System.exit(0);
			} else
				while (trials <= 3) {
					enteredpass = JOptionPane.showInputDialog("Enter Password");
					if (!Pass.equals(enteredpass) && trials < 3) {
						JOptionPane.showMessageDialog(null,
								"Incorrect Password");
						trials++;
					} else if ((trials == 3) && (!Pass.equals(enteredpass))) {
						System.exit(0);
					} else {
						logged = true;
						trials = 4;
					}
				}
		}
		while (logged) {
			Game game = new Game();
			game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			game.setVisible(true);
			logged = false;
		}
	}
}
