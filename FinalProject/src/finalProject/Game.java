package finalProject;

import java.awt.BorderLayout;
import java.awt.event.*;
import javax.swing.*;

public class Game extends JFrame {

	public static String command;
	public boolean sword = false;
	public boolean shield = false;
	public boolean goggles = false;
	public boolean key = false;
	public boolean treasure = false;
	public boolean map = false;

	JFrame frame = new JFrame("Game gui");
	static JTextField textInput = new JTextField();
	static JTextArea textOutputArea = new JTextArea(5, 50);
	JButton jbtnenter = new JButton("Enter");
	JPanel displayPanel = new JPanel();
	static int level = 1;
	JScrollPane scrollpane = new JScrollPane(textOutputArea);

	public Game() {
		jbtnenter.addActionListener(commandListener());
		textInput.addActionListener(commandListener());
		displayPanel.setLayout(new BorderLayout());

		textOutputArea.setEditable(false);

		displayPanel.add(textOutputArea, BorderLayout.NORTH);
		displayPanel.add(textInput, BorderLayout.CENTER);
		displayPanel.add(jbtnenter, BorderLayout.SOUTH);

		this.add(displayPanel);

		pack();
		setoText("Hello! There is an evil villain in this world! Please save us! First, find a weapon!");
	}

	static void setoText(String text) {
		textOutputArea.setText(text);
	}

	private ActionListener commandListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				processCommand();
			}
		};
	}

	public static String getCommand() {
		command = textInput.getText();
		command.toLowerCase();
		return command;
	}

	private void processCommand() {
		if (level == 1) {
			switch (getCommand()) {
			case "look":
				if (key == false) {
					setoText("A key! This might be useful.");
					key = true;
					Inventory.additem("key");
					break;
				} else {
					setoText("There is a nearby village");
					break;
				}
			case "go":
				setoText("You go to a nearby village. There is an old mansion in the village.");
				level++;
				break;
			case "help":
				Basemethod.basic();
				break;
			case "bag":
				Inventory.display();
				break;
			default:
				setoText("I don't understand. What are you trying to do?");
				break;
			}
		} else if (level == 2) {
			switch (getCommand()) {
			case "go":
				if (key == false) {
					setoText("You go to the mansion. It's abandoned and scary.");
					break;
				} else if (key) {
					setoText("The mansion is abandoned. In a room, there is a chest which you open with the key.\nYou got a magic sword!");
					sword = true;
					Inventory.additem("sword");
					break;
				}
			case "look":
				setoText("There is a library, and after looking around, you find a map.");
				map = true;
				Inventory.additem("map");
				break;
			case "leave":
				setoText("You leave the village. There is a forest nearby.");
				level++;
				break;
			case "help":
				Basemethod.basic();
				break;
			default:
				setoText("I don't understand. What are you trying to do?");
				break;
			}
		} else if (level == 3) {
			switch (getCommand()) {
			case "go":
				if (map) {
					setoText("You go to the forest. The map has two locations listed, S and T.");
					break;
				} else {
					setoText("You wander in circles, getting lost. You end up back in the village.");
					level = 2;
					break;
				}
			case "s":
				setoText("You find a shield!");
				shield = true;
				Inventory.additem("shield");
				break;
			case "t":
				setoText("You found treasure!");
				treasure = true;
				Inventory.additem("treasure");
				level = 4;
				break;
			case "help":
				Basemethod.basic();
				break;
			default:
				setoText("I don't understand. What are you trying to do?");
				break;
			}
		}
		else if (level == 4) {
			switch (getCommand()) {
			case "shop":
				if (treasure) {
					setoText("You return to the village to buy the glasses from the shop");
					goggles = true;
					Inventory.additem("goggles");
					level = 5;
					break;
				}
				else {
					setoText("You need to find the treasure before you can go back!");
					level = 3;
					break;
				}
			case "help":
				Basemethod.basic();
				break;
			default:
				setoText("I don't understand. What are you trying to do?");
				break;
			}
		}
		else if (level == 5) {
			switch (getCommand()) {
			case "boss":
				if (sword && shield && goggles) {
					setoText("You enter a cave to face the evil Lord McAfee and vanquish him! This is the end of the game. Click the x to close it. Thanks for playing!");
					break;
				}
				else {
					setoText("GAME OVER! You didn't have the neccesary tools to defeat Lord McAfee");
					break;
				}
			case "help":
				Basemethod.basic();
				break;
			default:
				setoText("I don't understand. What are you trying to do?");
				break;
			}
		}
	}
}