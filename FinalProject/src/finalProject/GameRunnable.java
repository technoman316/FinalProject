package finalProject;

import javax.swing.JFrame;

import finalProject.Game;

public class GameRunnable {

	public static void main(String[] args) {
		
		Game game = new Game();
        game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        game.setVisible(true);
	}
	}

